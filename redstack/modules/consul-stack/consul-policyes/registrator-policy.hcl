service "" {
    policy = "write"
}

service_prefix "" {
    policy = "write"
}

node_prefix "" {
    policy = "read"
}
